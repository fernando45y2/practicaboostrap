import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CitaRoutingModule } from './cita-routing.module';
import { CitaListComponent } from './cita-list/cita-list.component';
import { CitaCreateComponent } from './cita-create/cita-create.component';
import { CitaUpdateComponent } from './cita-update/cita-update.component';


@NgModule({
  declarations: [CitaListComponent, CitaCreateComponent, CitaUpdateComponent],
  imports: [
    CommonModule,
    CitaRoutingModule
  ]
})
export class CitaModule { }
