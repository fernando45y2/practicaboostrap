import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CitaCreateComponent } from './cita-create/cita-create.component';
import { CitaListComponent } from './cita-list/cita-list.component';
import { CitaUpdateComponent } from './cita-update/cita-update.component';
import { HeaderComponent } from '../header/header.component';


const routes: Routes = [
  {
    path:"",
    component:CitaCreateComponent
  },
  {
    path:"cita-create",
    component:CitaCreateComponent
  },
  {
    path:"cita-list",
    component:CitaListComponent
  },
  {
    path:"cita-update",
    component:CitaUpdateComponent
  }
 
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CitaRoutingModule { }
