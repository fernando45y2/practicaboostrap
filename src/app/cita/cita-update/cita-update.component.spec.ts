import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CitaUpdateComponent } from './cita-update.component';

describe('CitaUpdateComponent', () => {
  let component: CitaUpdateComponent;
  let fixture: ComponentFixture<CitaUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CitaUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CitaUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
